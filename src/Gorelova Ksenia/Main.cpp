#include "Matrix.h"
#include <fstream>

using namespace std;

int main()
{
	srand(time(0));
	ifstream file("Matrix.txt");
	ofstream newFile("new Matrix.txt");
	Matrix M1(3, 3), M2(3, 3);
	file >> M1 >> M2;
	newFile << M1 << endl << M2;
	newFile << endl << "The sum of the matrices" << endl << M1 + M2 << endl;
	newFile << endl << "The subtraction of matrices" << endl << M1 - M2 << endl;
	newFile << endl << "The multiplication of matrices" << endl << M1 * M2 << endl;
	newFile << "The multiplication first matrix by number " << 3 << endl << M1 * 3 << endl;
	newFile << "The devide first mutrix by number " << 3 << endl << M1 / 3 << endl;
	if (M1 == M2)
		newFile << "Of the matrix is equal" << endl;
	else if (M1 != M2)
		newFile << "Matrix isn`t equal" << endl;
	return 0;
}