#include "Matrix.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

Matrix::~Matrix()
{
	for (int i = 0; i < ROWS; i++)
	{
		delete[] matrix[i];
	}
	delete[] matrix;
}

Matrix::Matrix(int M, int N)
{
	ROWS = M;
	COLS = N;
	matrix = new double*[M];
	for (int i = 0; i < M; i++)
	{
		matrix[i] = new double[N];
	}
}

Matrix::Matrix(const Matrix & mat)
{
	ROWS = mat.ROWS;
	COLS = mat.COLS;
	matrix = new double*[ROWS];
	for (int i = 0; i < ROWS; i++)
		matrix[i] = new double[COLS];
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			matrix[i][j] = mat[i][j];
		}
	}
}

ostream & operator<<(ostream & stream, const Matrix & mat)
{
	for (int i = 0; i < mat.COLS; i++)
	{
		for (int j = 0; j < mat.ROWS; j++)
		{
			stream << mat(i, j) << " ";
		}
		stream << endl;
	}
	return stream;
}

istream & operator>>(istream & str, const Matrix & mat)
{
	for (int i = 0; i < mat.COLS; i++)
	{
		for (int j = 0; j < mat.ROWS; j++)
		{
			str >> mat(i, j);
		}
	}
	return str;
}

double *& Matrix::operator[](int index) const
{
	return matrix[index];
}

double & Matrix::operator()(int i, int j) const
{
	if (i < 0 || i >= ROWS || j < 0 || j >= COLS)
		throw 1;
	return matrix[i][j];
}

Matrix Matrix::operator+(const Matrix & mat)
{
	Matrix tmp(ROWS, COLS);
	if (ROWS != mat.ROWS || COLS != mat.COLS)
		throw 1;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			tmp(i, j) = matrix[i][j] + mat[i][j];
		}
	}
	return tmp;
}

Matrix Matrix::operator-(const Matrix & mat)
{
	Matrix tmp(ROWS, COLS);
	if (ROWS != mat.ROWS || COLS != mat.COLS)
		throw 1;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			tmp(i, j) = matrix[i][j] - mat[i][j];
		}
	}
	return tmp;
}

Matrix Matrix::operator*(const Matrix & mat)
{
	Matrix tmp(ROWS, COLS);
	if (COLS != mat.ROWS)
		throw 1;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < mat.COLS; j++)
		{
			tmp(i, j) = 0;
			for (int k = 0; k < COLS; k++)
			{
				tmp.matrix[i][j] += (matrix[i][k] * mat.matrix[k][j]);
			}
		}
	}
	return tmp;
}

Matrix Matrix::operator*(const double & mult)
{
	Matrix tmp(ROWS, COLS);
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			tmp(i, j) = matrix[i][j] * mult;
		}
	}
	return tmp;
}

Matrix Matrix::operator/(const double & div)
{
	Matrix tmp(ROWS, COLS);
	if (div == 0)
		throw 1;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			tmp(i, j) = matrix[i][j] / div;
		}
	}
	return tmp;
}

Matrix & Matrix::operator=(const Matrix & mat)
{
	if (&mat == this)
		return *this;
	if (mat.COLS != COLS || mat.ROWS != ROWS)
		throw 1;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			matrix[i][j] = mat[i][j];
		}
	}
	return *this;
}

bool Matrix::operator==(const Matrix & mat)
{
	if (ROWS != mat.ROWS || COLS != mat.COLS)
		return false;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			if (matrix[i][j] != mat[i][j])
				return false;
		}
	}
	return true;
}

bool Matrix::operator!=(const Matrix & mat)
{
	return !(*this == mat);
}

Matrix & Matrix::operator+=(const Matrix & mat)
{
	if (ROWS != mat.ROWS || COLS != mat.COLS)
		throw 1;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			matrix[i][j] += mat[i][j];
		}
	}
	return *this;
}

Matrix & Matrix::operator-=(const Matrix & mat)
{
	if (ROWS != mat.ROWS || COLS != mat.COLS)
		throw 1;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			matrix[i][j] -= mat[i][j];
		}
	}
	return *this;
}

Matrix & Matrix::operator*=(const double & m)
{
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			matrix[i][j] *= m;
		}
	}
	return *this;
}

Matrix & Matrix::operator/=(const double & m)
{
	if (m == 0)
		throw 1;
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			matrix[i][j] /= m;
		}
	}
	return *this;
}