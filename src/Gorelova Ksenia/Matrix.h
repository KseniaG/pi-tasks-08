#pragma once
#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;
class Matrix
{
private:
	int ROWS;
	int COLS;
	double **matrix;

public:
	friend ostream& operator<<(ostream& stream, const Matrix& mat);
	friend istream& operator>>(istream& str, const Matrix& mat);
	~Matrix();
	Matrix(int M, int N);
	Matrix(const Matrix & mat);
	double *& operator[](int index) const;
	double & operator()(int i, int j) const;
	Matrix operator+(const Matrix & mat);
	Matrix operator-(const Matrix & mat);
	Matrix operator*(const Matrix & mat);
	Matrix operator*(const double & mult);
	Matrix operator/(const double & div);
	Matrix & operator=(const Matrix & mat);
	bool operator==(const Matrix & mat);
	bool operator!=(const Matrix & mat);
	Matrix & operator+=(const Matrix & mat);
	Matrix & operator-=(const Matrix & mat);
	Matrix & operator*=(const double & m);
	Matrix & operator/=(const double & m);
};

